# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.1.0](https://gitlab.com/foundryvtt-pt-br/starfinder/compare/v1.0.0...v1.1.0) (2020-11-01)


### Features

* translate system and compendiums ([43fe87f](https://gitlab.com/foundryvtt-pt-br/starfinder/commit/43fe87fe482c87ad4cb255b3e637c556787b4676))

## 1.0.0 (2020-10-22)


### Features

* initial release ([63e965f](https://gitlab.com/foundryvtt-pt-br/starfinder/commit/63e965f9d27fd73ead456b81db5ff5bf3d8cb54b))

## 1.0.0 (2020-10-22)


### Features

* initial release ([28e0b80](https://gitlab.com/foundryvtt-pt-br/starfinder/commit/28e0b80f22d1a970026fc3074f52add36628df02))
