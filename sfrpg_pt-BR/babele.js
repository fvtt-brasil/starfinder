Hooks.on('init', () => {
    if (typeof Babele !== 'undefined') {
        Babele.get().register({
            module: 'sfrpg_pt-BR',
            lang: 'pt-BR',
            dir: 'compendium'
        })
    }
})
